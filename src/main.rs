use std::cmp::Ordering;
use std::cmp::Ordering::*;

// Numerically compare two strings like "002385.299283" or "2938375".
fn numcmp(a: &str, b: &str) -> Ordering {
    // Consume zeroes from both strings
    let mut a_chars = a.chars().skip_while(|c| *c == '0');
    let mut b_chars = b.chars().skip_while(|c| *c == '0');

    // At this point we don't know how many chars until the decimal point.
    // Obviously whichever string has more significant digits before '.' is larger.
    // We proceed with the comparison assuming that both are equal length,
    // but won't return our result until we confirm that fact.

    // not used after the decimal point
    let mut cmp_result = Equal;

    // Counterintutively, we only need one flag because both numbers will reach the decimal point at the same time
    // or we will return early.
    let mut passed_decimal = false;

    loop {
        let ach = a_chars.next();
        let bch = b_chars.next();

        println!("{:?}, {:?}", ach, bch);

        if !passed_decimal {
            match (ach, bch) {
                // both numbers reached decimal point or pre-decimal end of string at the same time
                (Some('.'), Some('.')) | (None, None) => {
                    passed_decimal = true;

                    // We've confirmed that both strings have equal numbers of digits before the decimal
                    // If a difference was noted in earlier digits, we can now return that result
                    if cmp_result != Equal {
                        return cmp_result;
                    }

                    // Numbers were equal up to this point. Continue...
                }

                // a reaches decimal point or end of string first
                (Some('.'), Some(_bch)) | (None, Some(_bch)) => {
                    // a < b because a is shorter than b
                    return Less;
                }

                // b reaches decimal point or end of string first
                (Some(_ach), Some('.')) | (Some(_ach), None) => {
                    // a > b because a is longer than b
                    return Greater;
                }

                // Two normal digits, continue comparing
                (Some(ach), Some(bch)) => {
                    if cmp_result == Equal {
                        cmp_result = ach.cmp(&bch);
                    }

                    // A difference was noted in an earlier digit, but we can't return it
                    // until we finish counting digits to the decimal point (or end of string)
                }
            }
        } else {
            match (ach, bch) {
                // Both numbers have reached the end.
                (None, None) => {
                    // We would not be here unless both numbers were equal up till now
                    return Equal;
                }

                // a reaches the end first
                (None, Some(bch)) => {
                    // a < b because b has a non-zero decimal digit after the end of a
                    if bch != '0' {
                        return Less;
                    }

                    // bch == 0.  We might be seeing trailing zeros at this point so we can't return just yet...
                }

                // b reaches the end first
                (Some(ach), None) => {
                    // a > b because a has a non-zero decimal digit after the end of b
                    if ach != '0' {
                        return Greater;
                    }

                    // ach == 0.  We might be seeing trailing zeros at this point so we can't return just yet...
                }

                // Two normal digits after the decimal point, continue comparing
                (Some(ach), Some(bch)) => {
                    match ach.cmp(&bch) {
                        Equal => (),             // continue comparing
                        result => return result, // we can safely early return
                    }
                }
            }
        }
    }
}

fn main() {
    println!("{:?}", numcmp("00183.393", "920.20"));
}
